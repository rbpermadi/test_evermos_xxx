<?php
include_once 'amunition.php';

class Gun{
 
    // database connection and table name
    private $conn;
    private $table_name = "guns";
 
    // object properties
    public $id;
    public $total_slot_magazines;
    public $created_at;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // create gun
	function create(){
	 
	    // query to insert record
	    $query = "INSERT INTO
	                " . $this->table_name . "
	            SET
	                total_slot_magazines=:total_slot_magazines, created_at=:created_at";
	 
	    // prepare query
	    $stmt = $this->conn->prepare($query);
	 
	    // sanitize
	    $this->total_slot_magazines=htmlspecialchars(strip_tags($this->total_slot_magazines));
	 
	    // bind values
	    $stmt->bindParam(":total_slot_magazines", $this->total_slot_magazines);
	    $stmt->bindParam(":created_at", $this->created_at);
	 
	    // execute query
	    if($stmt->execute()){
	    	$this->id = $this->conn->lastInsertId();
	        return true;
	    }
	 
	    return false;
	     
	}

	// used when filling up the update product form
	function readOne(){
	 
	    // query to read single record
	    $query = "SELECT
	                g.id, g.total_slot_magazines, g.created_at
	            FROM
	                " . $this->table_name . " g
	            WHERE
	                g.id = ?
	            LIMIT
	                0,1";
	 
	    // prepare query statement
	    $stmt = $this->conn->prepare( $query );
	 
	    // bind id of product to be updated
	    $stmt->bindParam(1, $this->id);
	 
	    // execute query
	    $stmt->execute();
	 
	    // get retrieved row
	    $row = $stmt->fetch(PDO::FETCH_ASSOC);

	    // set values to object properties
	    $this->total_slot_magazines = $row['total_slot_magazines'];
	    $this->created_at = $row['created_at'];
	}

    // load magazine into gun.
    public function loadMagazines(){
    	$result = null;
    	
    	for($i=0; $i < $this->total_slot_magazines; $i++){
    		
    		$a = new Amunition();

    		if ($a->fillBulletIntoMagazine()){
    			$result = $i;
    			break;
    		}
    	}

    	return $result;
    }
}
?>