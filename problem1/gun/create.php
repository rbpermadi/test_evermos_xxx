<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../../config/database.php';
 
// instantiate product object
include_once '../../objects/gun.php';
 
$database = new Database();
$db = $database->getConnection();
 
$gun = new Gun($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// make sure data is not empty
if( !empty($data->total_slot_magazines) ){
 
    // set product property values
    $gun->total_slot_magazines = $data->total_slot_magazines;
    $gun->created_at = date('Y-m-d H:i:s');
 
    // create the gun
    if($gun->create()){
 
        // set response code - 201 created
        http_response_code(201);
 		$gun_arr = array(
 			"id" => $gun->id,
 			"total_slot_magazines" => $gun->total_slot_magazines,
 			"created_at" => $gun->created_at
 		);
        // tell the user
        echo json_encode(array("data" => $gun_arr, "message" => "Product was created."));
    }
 
    // if unable to create the gun, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Unable to create gun."));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to create gun. Data is incomplete."));
}
?>