<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
// header('Content-Type: application/json');
 
// include database and object files
include_once '../../config/database.php';
include_once '../../objects/gun.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare gun object
$gun = new Gun($db);
 
// set ID property of record to read
$gun->id = isset($_GET['id']) ? $_GET['id'] : die();
 
// read the details of gun to be edited
$gun->readOne();

if($gun->total_slot_magazines!=null){
    // load magazine
    $verified_slot_magazine = $gun->loadMagazines();
    
    if($verified_slot_magazine !== null){
        // set response code - 200 OK
        http_response_code(200);
     
        // make it json format
        echo json_encode(array("verified_slot_magazine" => $verified_slot_magazine));
    }

    else {
        // set response code - 422 Unprocessable Entity
        http_response_code(422);
     
        // tell the user there are no full magazine
        echo json_encode(array("message" => "There are no full magazine."));
    }
}
 
else{
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user gun does not exist
    echo json_encode(array("message" => "Gun does not exist."));
}
?>